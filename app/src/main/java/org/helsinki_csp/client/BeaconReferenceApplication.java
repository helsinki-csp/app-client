package org.helsinki_csp.client;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;


import android.os.Build;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.powersave.BackgroundPowerSaver;
import org.altbeacon.beacon.startup.RegionBootstrap;
import org.altbeacon.beacon.startup.BootstrapNotifier;
import org.helsinki_csp.client.R;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class BeaconReferenceApplication extends Application implements BootstrapNotifier {

    private static final String TAG = "BeaconReferenceApp";
    private RegionBootstrap regionBootstrap;
    private BackgroundPowerSaver backgroundPowerSaver;
    private MonitoringActivity monitoringActivity = null;
    private String cumulativeLog = "";
    private JSONArray beaconFile;
    private HashMap<String, String> debugViewItemsMap = new HashMap<>();

    public static String backendUid = null;
    public void onCreate() {
        super.onCreate();

        try {
            this.beaconFile = getBeaconDataFromDisk();
            Log.d(TAG,"Parsed JSON from disk");
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG,"Can't open JSON");
        } catch (JSONException e) {
            Log.d(TAG,"Can't parse JSON");
            e.printStackTrace();
        }
        BeaconManager beaconManager = org.altbeacon.beacon.BeaconManager.getInstanceForApplication(this);

        // By default the AndroidBeaconLibrary will only find AltBeacons.  If you wish to make it
        // find a different type of beacon, you must specify the byte layout for that beacon's
        // advertisement with a line like below.  The example shows how to find a beacon with the
        // same byte layout as AltBeacon but with a beaconTypeCode of 0xaabb.  To find the proper
        // layout expression for other beacon types, do a web search for "setBeaconLayout"
        // including the quotes.
        //

        // should be the iBeacon format
        beaconManager.getBeaconParsers().clear();
        beaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));

        beaconManager.setDebug(true);


        // Uncomment the code below to use a foreground service to scan for beacons. This unlocks
        // the ability to continually scan for long periods of time in the background on Andorid 8+
        // in exchange for showing an icon at the top of the screen and a always-on notification to
        // communicate to users that your app is using resources in the background.
        //


        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setContentTitle("HSL Beacon service is active");
        Intent intent = new Intent(this, MonitoringActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT
        );
        builder.setContentIntent(pendingIntent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("Beacon Reference Notification",
                    "HSL beacon service notifications", NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("All notifications concerning HSL beacons");
            NotificationManager notificationManager = (NotificationManager) getSystemService(
                    Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
            builder.setChannelId(channel.getId());
        }
        beaconManager.enableForegroundServiceScanning(builder.build(), 456);

        // For the above foreground scanning service to be useful, you need to disable
        // JobScheduler-based scans (used on Android 8+) and set a fast background scan
        // cycle that would otherwise be disallowed by the operating system.
        //
        beaconManager.setEnableScheduledScanJobs(false);
        beaconManager.setBackgroundBetweenScanPeriod(0);
        beaconManager.setBackgroundScanPeriod(1100);


        Log.d(TAG, "setting up background monitoring for beacons and power saving");
        // wake up the app when a beacon is seen
        Region region = new Region("backgroundRegion",
                Identifier.parse("DFFF7ADA-A48A-4F77-AA9A-3A7943641E6C"), null, null);
        regionBootstrap = new RegionBootstrap(this, region);

        // simply constructing this class and holding a reference to it in your custom Application
        // class will automatically cause the BeaconLibrary to save battery whenever the application
        // is not visible.  This reduces bluetooth power usage by about 60%
        backgroundPowerSaver = new BackgroundPowerSaver(this);

        // If you wish to test beacon detection in the Android Emulator, you can use code like this:
        BeaconManager.setBeaconSimulator(new TimedBeaconSimulator() );
        ((TimedBeaconSimulator) BeaconManager.getBeaconSimulator()).createTimedSimulatedBeacons();

        putToDebugViewList("latestLogToDisplay", "Latest log-to-display: Nothing");

    }

    public void disableMonitoring() {
        if (regionBootstrap != null) {
            regionBootstrap.disable();
            regionBootstrap = null;
        }
    }
    public void enableMonitoring() {
        Region region = new Region("backgroundRegion",
                Identifier.parse("DFFF7ADA-A48A-4F77-AA9A-3A7943641E6C"), null, null);
        regionBootstrap = new RegionBootstrap(this, region);
    }


    @Override
    public void didEnterRegion(Region arg0) {
        Log.d(TAG, "did enter region.");
        // Send a notification to the user whenever a Beacon
        // matching a Region (defined above) are first seen.
        Log.d(TAG, "Sending notification.");
        sendNotification();

        // todo: send all beacons in region?
        Log.d(TAG, "Queuing sending to server");
        sendBeaconToServer(0x0007, 0x0001);
        sendBeaconToServer(0x0060, 0x0002);
        sendBeaconToServer(0x0007, 0x0001);

        if (monitoringActivity != null) {
            // If the Monitoring Activity is visible, we log info about the beacons we have
            // seen on its display
            logToDisplay("Beacon found: " + arg0.getId2() + " " + arg0.getId3() );
        }
    }

    public static void getUid() {
        if(backendUid == null) {
            AsyncHttpClient client = new AsyncHttpClient();
            client.post("http://hsl-traffic.herokuapp.com/auth", null, new TextHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, String res) {
                            // called when response HTTP status is "200 OK"
                            backendUid = res;
                            Log.d(TAG, "UID received!" + res);
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                            Log.d(TAG, "Cannot authenticate! Status: " + statusCode);
                        }
                    }
            );
        }
    }
    public void sendBeaconToServer(final int major, final int minor) {
        getUid(); // todo: can't really start anything until uid is known
        AsyncHttpClient client = new AsyncHttpClient();
        final String beaconData = Integer.toHexString(major) + ":" + Integer.toHexString(minor);
        final RequestParams params = new RequestParams();
        params.put("auth", backendUid);
        params.put("beac", beaconData);
        client.post("http://hsl-traffic.herokuapp.com/beac", params, new TextHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String res) {
                        // called when response HTTP status is "200 OK"
                        Log.d(TAG,"Beacon data sent to server" );
                        Log.d(TAG, "UID: "+backendUid);
                        Log.d(TAG,"Params: "+ (params.toString()));
                        Log.d(TAG, "Response: "+res);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                        Log.d(TAG,"Beacon data not sent to server! Status: " + statusCode);
                    }
                }
        );
    }
    private JSONArray getBeaconDataFromDisk() throws IOException, JSONException {
        String json = null;
        try {
            InputStream is = getBaseContext().getResources().openRawResource(R.raw.hsl);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return new JSONArray(json);

    }
    public String getStopNameFromBeacon(int major, int minor) {
        try {
            for(int i = 0; i < beaconFile.length(); i++) {
                JSONObject o = beaconFile.getJSONObject(i);
                if(major == Integer.parseInt(o.getString("major"), 16)
                        && minor == Integer.parseInt(o.getString("minor"),16)) {
                    return o.getString("serial");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
    public String getStopIdFromBeacon(int major, int minor) {
        try {
            for(int i = 0; i < beaconFile.length(); i++) {
                JSONObject o = beaconFile.getJSONObject(i);
                if(major == Integer.parseInt(o.getString("major"), 16)
                        && minor == Integer.parseInt(o.getString("minor"),16)) {
                    return o.getString("stop");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void didExitRegion(Region region) {
        logToDisplay("Beacon lost");
    }

    @Override
    public void didDetermineStateForRegion(int state, Region region) {
        logToDisplay("You are now " + (state == 1 ? "inside" : "outside") + " the zone");
    }

    private void sendNotification() {
        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("Beacon Reference Notifications",
                    "Beacon Reference Notifications", NotificationManager.IMPORTANCE_HIGH);
            channel.enableLights(true);
            channel.enableVibration(true);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            notificationManager.createNotificationChannel(channel);
            builder = new Notification.Builder(this, channel.getId());
        }
        else {
            builder = new Notification.Builder(this);
            builder.setPriority(Notification.PRIORITY_HIGH);
        }

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntent(new Intent(this, MonitoringActivity.class));
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setContentTitle("HSL beacon detected!");
        builder.setContentText("View details");
        builder.setContentIntent(resultPendingIntent);
        notificationManager.notify(1, builder.build());
    }

    public void setMonitoringActivity(MonitoringActivity activity) {
        this.monitoringActivity = activity;
    }

    private void logToDisplay(String line) {
        cumulativeLog += (line + "\n");
        if (this.monitoringActivity != null) {
            this.monitoringActivity.updateLog(cumulativeLog);
        }
        putToDebugViewList("latestLogToDisplay", "Latest log-to-display: " + line);
    }

    public String getLog() {
        return cumulativeLog;
    }

    public List<String> getDebugViewList() {
        return new ArrayList<String>(debugViewItemsMap.values());
    }

    public void putToDebugViewList (String key, String value){
        debugViewItemsMap.put(key, value);
    }


}

