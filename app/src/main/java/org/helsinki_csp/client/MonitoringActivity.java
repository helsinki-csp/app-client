package org.helsinki_csp.client;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.RemoteException;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.helsinki_csp.client.R;
import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.ITileSource;
import org.osmdroid.tileprovider.tilesource.TMSOnlineTileSourceBase;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.util.MapTileIndex;
import org.osmdroid.views.CustomZoomButtonsController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import android.widget.TextView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import cz.msebera.android.httpclient.Header;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class MonitoringActivity extends AppCompatActivity implements BeaconConsumer, LocationListener {
	protected static final String TAG = "MonitoringActivity";
	private static final int PERMISSION_REQUEST_FINE_LOCATION = 1;
	private static final int PERMISSION_REQUEST_BACKGROUND_LOCATION = 2;

	private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";
	private BeaconManager beaconManager = BeaconManager.getInstanceForApplication(this);
	private MapView map;
	private MyLocationNewOverlay mLocationOverlay;
	private List<Marker> mapMarkers = new ArrayList<>();
	private List<NearStopsQuery.Edge> nearestStops;


	protected LocationManager locationManager;
	protected LocationListener locationListener;
	protected double latitude = 60.17;
	protected double longitude = 24.937;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_monitoring);
		//Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		// Sets the Toolbar to act as the ActionBar for this Activity window.
		// Make sure the toolbar exists in the activity and is not null
		//setActionBar(toolbar);
		verifyBluetooth();

		setStopName("No Stop Detected");

		// create map
		map = (MapView) findViewById(R.id.map);
		//map.setTileSource(TileSourceFactory.MAPNIK); // todo: set HSL's

		final ITileSource tileSource = new TMSOnlineTileSourceBase("HSL", 1, 20, 256, ".png",
				new String[]{"https://digitransit-prod-cdn-origin.azureedge.net/map/v1/hsl-map-256/", "https://cdn.digitransit.fi/map/v1/hsl-map-256/"}) {
			@Override
			public String getTileURLString(long pMapTileIndex) {
				String tileUrl = getBaseUrl()
						+ MapTileIndex.getZoom(pMapTileIndex)
						+ "/" + MapTileIndex.getX(pMapTileIndex)
						+ "/" + MapTileIndex.getY(pMapTileIndex)
						+ mImageFilenameEnding;
				return tileUrl;
			}
		};
		map.setTileSource(tileSource);

		map.getZoomController().setVisibility(CustomZoomButtonsController.Visibility.ALWAYS);
		map.setMultiTouchControls(true);
		IMapController mapController = map.getController();
		mapController.setZoom(18);
		GeoPoint startPoint = new GeoPoint(60.1696, 24.93545);
		mapController.setCenter(startPoint);

		Configuration.getInstance().setUserAgentValue(getApplicationContext().getPackageName());

		// person marker
		this.mLocationOverlay = new MyLocationNewOverlay(new GpsMyLocationProvider(getApplicationContext()),map);
		this.mLocationOverlay.enableMyLocation();
		this.mLocationOverlay.setDrawAccuracyEnabled(true);
		this.mLocationOverlay.setDirectionArrow(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.person), BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.person));
		map.getOverlays().add(this.mLocationOverlay);

		//addMarker("A random bus nearby", 60.17, 24.937, R.drawable.ic_launcher);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if (this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
					== PackageManager.PERMISSION_GRANTED) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
					if (this.checkSelfPermission(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
							!= PackageManager.PERMISSION_GRANTED) {
						if (!this.shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_BACKGROUND_LOCATION)) {
							final AlertDialog.Builder builder = new AlertDialog.Builder(this);
							builder.setTitle("This app needs background location access");
							builder.setMessage("Please grant location access so this app can detect beacons in the background.");
							builder.setPositiveButton(android.R.string.ok, null);
							builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

								@TargetApi(23)
								@Override
								public void onDismiss(DialogInterface dialog) {
									requestPermissions(new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION},
											PERMISSION_REQUEST_BACKGROUND_LOCATION);
								}

							});
							builder.show();
						}
						else {
							final AlertDialog.Builder builder = new AlertDialog.Builder(this);
							builder.setTitle("Functionality limited");
							builder.setMessage("Since background location access has not been granted, this app will not be able to discover beacons in the background.  Please go to Settings -> Applications -> Permissions and grant background location access to this app.");
							builder.setPositiveButton(android.R.string.ok, null);
							builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

								@Override
								public void onDismiss(DialogInterface dialog) {
								}

							});
							builder.show();
						}
					}
				}
			} else {
				if (!this.shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
					ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
									Manifest.permission.ACCESS_BACKGROUND_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE},
							PERMISSION_REQUEST_FINE_LOCATION);
				}
				else {
					final AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setTitle("Functionality limited");
					builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons.  Please go to Settings -> Applications -> Permissions and grant location access to this app.");
					builder.setPositiveButton(android.R.string.ok, null);
					builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

						@Override
						public void onDismiss(DialogInterface dialog) {
						}

					});
					builder.show();
				}

			}
		}

		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		try {
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		} catch(Exception e) {	}
	}

	public void addMarker(final String title, final double lat, final double lon, final int drawable) {
		runOnUiThread(new Runnable() {
			public void run() {

				Marker startMarker = new Marker(map);
				startMarker.setTitle(title);
				startMarker.setIcon(getDrawable(drawable));
				startMarker.setPosition(new GeoPoint(lat, lon));
				startMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
				map.getOverlays().add(startMarker);
				mapMarkers.add(startMarker);
			}
		});
	}
	public void deleteMarkers() {
		runOnUiThread(new Runnable() {
			public void run() {
				Iterator<Marker> i = mapMarkers.iterator();

				while (i.hasNext()) {
					Marker m = i.next();
					map.getOverlays().remove(m);
					i.remove();
				}
			}
		});
	}

	@Override
	public void onRequestPermissionsResult(int requestCode,
										   String permissions[], int[] grantResults) {
		switch (requestCode) {
			case PERMISSION_REQUEST_FINE_LOCATION: {
				if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					Log.d(TAG, "fine location permission granted");
				} else {
					final AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setTitle("Functionality limited");
					builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons.");
					builder.setPositiveButton(android.R.string.ok, null);
					builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

						@Override
						public void onDismiss(DialogInterface dialog) {
						}

					});
					builder.show();
				}
				return;
			}
			case PERMISSION_REQUEST_BACKGROUND_LOCATION: {
				if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					Log.d(TAG, "background location permission granted");
				} else {
					final AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setTitle("Functionality limited");
					builder.setMessage("Since background location access has not been granted, this app will not be able to discover beacons when in the background.");
					builder.setPositiveButton(android.R.string.ok, null);
					builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

						@Override
						public void onDismiss(DialogInterface dialog) {
						}

					});
					builder.show();
				}
				return;
			}
		}
	}

    @Override
    public void onResume() {
        super.onResume();
        map.onResume();
        BeaconReferenceApplication application = ((BeaconReferenceApplication) this.getApplicationContext());
        application.setMonitoringActivity(this);
        updateLog(application.getLog());
        beaconManager.bind(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        map.onPause();
        ((BeaconReferenceApplication) this.getApplicationContext()).setMonitoringActivity(null);
		beaconManager.unbind(this);

	}

	private void verifyBluetooth() {

		try {
			if (!BeaconManager.getInstanceForApplication(this).checkAvailability()) {
				final AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("Bluetooth not enabled");
				builder.setMessage("Please enable bluetooth in settings and restart this application.");
				builder.setPositiveButton(android.R.string.ok, null);
				builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
					@Override
					public void onDismiss(DialogInterface dialog) {
						//finish();
			            //System.exit(0);
					}
				});
				builder.show();
			}
		}
		catch (RuntimeException e) {
			final AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Bluetooth LE not available");
			builder.setMessage("Sorry, this device does not support Bluetooth LE.");
			builder.setPositiveButton(android.R.string.ok, null);
			builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
					//finish();
		            //System.exit(0);
				}

			});
			builder.show();

		}

	}

    public void updateLog(final String log) {
    	runOnUiThread(new Runnable() {
    	    public void run() {
    	    	EditText editText = (EditText)MonitoringActivity.this
    					.findViewById(R.id.monitoringText);
       	    	editText.setText(log);
    	    }
    	});
    }




	@Override
	public void onPointerCaptureChanged(boolean hasCapture) {

	}

	@Override
	public void onBeaconServiceConnect() {

		RangeNotifier rangeNotifier = new RangeNotifier() {
			@Override
			public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
				if (beacons.size() > 0) {
					Log.d(TAG, "didRangeBeaconsInRegion called with beacon count:  "+beacons.size());
					Beacon firstBeacon = beacons.iterator().next();
					try {
						final String stopName = ((BeaconReferenceApplication) getApplication()).getStopNameFromBeacon(firstBeacon.getId2().toInt(), firstBeacon.getId3().toInt());
						((BeaconReferenceApplication) getApplication()).sendBeaconToServer(firstBeacon.getId2().toInt(), firstBeacon.getId3().toInt());
						setStopName(stopName);

						RequestParams params = new RequestParams();
						//final String stopId = Integer.toHexString(firstBeacon.getId2().toInt()) + ":" + Integer.toHexString(firstBeacon.getId3().toInt());
						params.put(
								((BeaconReferenceApplication) getApplication())
										.getStopIdFromBeacon(firstBeacon.getId2().toInt(), firstBeacon.getId3().toInt()), 0
						);
						AsyncHttpClient client = new AsyncHttpClient();
						client.post("http://hsl-traffic.herokuapp.com/occu", params, new JsonHttpResponseHandler() {
									@Override
									public void onSuccess(int statusCode, Header[] headers, JSONObject res) {
										deleteMarkers();
										String nPersons = null;
										try {
											nPersons = ""+res.getString(res.keys().next());
										} catch (JSONException e) {
											e.printStackTrace();
										}

										addMarker(stopName + " (" + nPersons + " persons)", 60.169 + Math.random() * 0.003, 24.937  + Math.random() * 0.003, R.drawable.osm_ic_follow_me_on);

										updateNearStops(latitude, longitude);
									}

									@Override
									public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
										Log.d(TAG,"No occupancy data found! Status: " + statusCode);
									}
								}
						);


					} catch(Exception e) {
						// ...
					}
				} else {
					((TextView)findViewById(R.id.stopName)).setText("No Stop Detected");
				}
			}

		};
		try {
			beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", Identifier.parse("DFFF7ADAA48A4F77AA9A3A7943641E6C"), null, null));
			beaconManager.addRangeNotifier(rangeNotifier);
			//beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
			//beaconManager.addRangeNotifier(rangeNotifier);
		} catch (RemoteException e) {   }
	}


	private void logToDisplay(final String line) {
		runOnUiThread(new Runnable() {
			public void run() {
				EditText editText = (EditText)MonitoringActivity.this.findViewById(R.id.monitoringText);
				editText.append(line+"\n");
			}
		});
	}
	private void setStopName(final String stopName) {
		runOnUiThread(new Runnable() {
			public void run() {
				((TextView)findViewById(R.id.stopName)).setText(stopName);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.top_right_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
			case R.id.menuItemBackgroundInfo:
				Intent myIntent = new Intent(this, BackgroundInfoActivity.class);
				startActivity(myIntent);
				return true;
			case R.id.menuItemDebugView:
				Intent myIntent2 = new Intent(this, DebugViewActivity.class);
				startActivity(myIntent2);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}


	@Override
	public void onLocationChanged(Location location) {
		this.latitude = location.getLatitude();
		this.longitude = location.getLongitude();

		updateNearStops(latitude, longitude);

		//txtLat = (TextView) findViewById(R.id.textview1);
		//txtLat.setText("Latitude:" + location.getLatitude() + ", Longitude:" + location.getLongitude());
	}

	private void updateNearStops(double latitude, double longitude) {

		// caching logic broken - only update once - but works as placeholder for future implementation :)
		// e.g. check if "old lat / lon" == new one
		if(nearestStops != null) {
			for (NearStopsQuery.Edge e : nearestStops) {
				NearStopsQuery.Stop stop = e.node().stop;
				addMarker(stop.name, stop.lat, stop.lon, R.drawable.zoom_in);
				// wtf is the "cursor" attribute?
			}
		}
			ApolloClient apolloClient = ApolloClient.builder()
					.serverUrl("https://api.digitransit.fi/routing/v1/routers/hsl/index/graphql")
					.build();

			apolloClient.query(new NearStopsQuery(latitude, longitude))
					.enqueue(new ApolloCall.Callback<NearStopsQuery.Data>() {
						@Override
						public void onResponse(@NotNull Response<NearStopsQuery.Data> response) {
							nearestStops = response.getData().stopsByRadius.edges();
						}

						@Override
						public void onFailure(@NotNull ApolloException e) {
							Log.e(TAG, "GraphQL error: ", e);
						}
					});
	}

	@Override
	public void onProviderDisabled(String provider) {
		Log.d("Latitude","disable");
	}

	@Override
	public void onProviderEnabled(String provider) {
		Log.d("Latitude","enable");
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		Log.d("Latitude","status");
	}

}
