package org.helsinki_csp.client;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import org.altbeacon.beacon.BeaconManager;
import org.helsinki_csp.client.R;

public class BackgroundInfoActivity extends AppCompatActivity {

    Button enableButton;

    protected static final String TAG = "BackgroundInfoActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_background_info);
        enableButton = (Button) findViewById(R.id.backgroundInfoMonitoringButton);
        if (BeaconManager.getInstanceForApplication(this).getMonitoredRegions().size() > 0) {
            enableButton.setText("Disable monitoring");
        } else {
            enableButton.setText("Enable monitoring");
        }
    }

    public void onEnableClicked(View view) {
        Log.d(TAG, "onEnableClicked");
        BeaconReferenceApplication application = ((BeaconReferenceApplication) this.getApplicationContext());
        if (BeaconManager.getInstanceForApplication(this).getMonitoredRegions().size() > 0) {
            application.disableMonitoring();
            enableButton.setText("Re-Enable Monitoring");
        } else {
            enableButton.setText("Disable Monitoring");
            application.enableMonitoring();

        }
    }
}