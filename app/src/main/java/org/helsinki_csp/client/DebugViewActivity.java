package org.helsinki_csp.client;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import org.helsinki_csp.client.R;

import java.util.List;

/*


Usage from the BeaconReferenceApplication Class:
putToDebugViewList("latestLogToDisplay", "Latest log-to-display: Nothing");

Usage from other classes:
((BeaconReferenceApplication) this.getApplication()).putToDebugViewList("latestLogToDisplay", "Latest log-to-display: Nothing");


*/

public class DebugViewActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debug_view);
        final ListView lv = (ListView) findViewById(R.id.debugViewList);
        final List<String> dbgItemsList = ((BeaconReferenceApplication) this.getApplication()).getDebugViewList();
        final ArrayAdapter<String> arrayAdapter
                = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, dbgItemsList);
        lv.setAdapter(arrayAdapter);
        arrayAdapter.notifyDataSetChanged();
    }
}